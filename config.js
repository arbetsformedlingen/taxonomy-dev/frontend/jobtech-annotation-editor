var g_app_taxonomy_api_path = "https://api-frontend-taxonomy-api-gitops.apps.testing.services.jtech.se/v1/taxonomy";
var g_app_annotation_api_path = "https://mentor-api-mentor-api-test.apps.testing.services.jtech.se";
var g_app_recommender_api_path = "https://semantic-concept-search-test.apps.testing.services.jtech.se";
var g_app_cors_path = "http://annotation-editor-cors-jobtech-annotation-editor-testing.apps.testing.services.jtech.se";
var g_app_git_project_id = "34666978";