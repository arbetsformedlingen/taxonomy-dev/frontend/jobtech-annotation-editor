import React from 'react';

class AnnotateLoading extends React.Component { 

	constructor() {
        super();        
    }

    render() {
        return (
			<div className="annotation_result">
				<div className="loading">
					<div>
						Laddar...
					</div>
					<div>
						<div className="loader">
							<div className="loader_bar"></div>
							<div className="loader_bar"></div>
							<div className="loader_bar"></div>
							<div className="loader_bar"></div>
							<div className="loader_bar"></div>
							<div className="loader_ball"></div>
						</div>
					</div>
				</div>
			</div>
        );
    }
	
}

export default AnnotateLoading