import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import EditUploadList from "./edit_upload_list"
import React from "react"

describe("EditUploadList", () => {
  test("renders", () => {
    render(<EditUploadList />);
    expect(screen.getByText(/Bearbeta annonser/)).toBeDefined();
    expect(screen.getByText(/Dra och släpp filer här/)).toBeDefined();
    expect(screen.getByText(/Välj fil/)).toBeDefined();
    expect(screen.getByText(/Stäng/)).toBeDefined();
    expect(EditUploadList).toBeInstanceOf(Function);
    expect(EditUploadList).toHaveProperty("prototype");
    expect(EditUploadList.prototype).toBeInstanceOf(Object);
    expect(EditUploadList.prototype.constructor).toBe(EditUploadList);
  });
});