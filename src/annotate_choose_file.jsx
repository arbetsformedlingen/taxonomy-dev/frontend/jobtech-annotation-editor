import React from 'react';
import Rest from './context/rest.jsx';
import Context from './context/context.jsx';
import Util from './context/util.jsx';
import Button from './controls/button.jsx';
import EventDispatcher from './context/event_dispatcher.jsx';
import Constants from './context/constants.jsx';
import EditUploadList from './edit_upload_list.jsx';
import { parseEDNString } from 'edn-data';

class AnnotateChooseFile extends React.Component { 

	constructor() {
        super();
		this.ITEMS_PER_PAGE = 50;
        this.state = {
			data: [],
			page: 0,
			isShowingUploadDialog: false,
        };
		this.firstSelected = null;       
    }

    componentDidMount() {		
        if(!Context.isFetchingFiles) {
			this.onFilesFeteched();
		} else {
			Context.onFetchingFilesComplete = this.onFilesFeteched.bind(this);
		}
		Context.onFetchingFileDataUpdated = this.forceUpdate.bind(this);
    }

	componentWillUnmount() {
		Context.onFetchingFilesComplete = null;
		Context.onFetchingFileDataUpdated = null;
	}

	onProcessListClicked() {
		this.setState({isShowingUploadDialog: true});
	}

	onFilesFeteched() {
		this.setState({data: Context.files}, () => {
			this.onPageChanged();
		});
	}

	onKeyUp(item, e) {
		if(e.key == "Enter") {
			this.onFileClicked(item);
		}
	}

	onFileClicked(item) {
		Context.visitFile(item);
	}

	onPageKeyUp(page, e) {
		if(e.key == "Enter") {
			this.onPageClicked(page);
		}
	}

	onPageClicked(page) {
		this.setState({page: page}, () => {
			document.getElementById("items").scrollTop = 0;
			this.onPageChanged();
		});
	}

	onPageChanged() {
		Context.fetchFileData(this.getPageItems(this.state.page));	
	}

	getPageItems(page) {
		var from = page * this.ITEMS_PER_PAGE;
		var to = from + this.ITEMS_PER_PAGE;
		if(to > this.state.data.length) {
			to = this.state.data.length;
		}
		// TODO: sort data based on column
		return this.state.data.slice(from, to);
	}
	
	renderUploadListDialog() {
		return (
			<div className="edit_overlay">
				<div className="edit_dialog">
					<EditUploadList
						onCloseDialog={() => {
							this.setState({isShowingUploadDialog: false})
						}}/>
				</div>
			</div>
		);
	}

	renderPageSelector() {
		var numberOfPages = Math.ceil(this.state.data.length / this.ITEMS_PER_PAGE);
		var pages = [];
		var dottsLeft = false;
		var dottsRight = false;
		var nrLeft = this.state.page < 5 ? 5 + (5 - this.state.page) : 5;
		var nrRight = (numberOfPages - this.state.page) < 5 ? 5 + (5 - (numberOfPages - this.state.page - 1)) : 5;
		for(var i=0; i<numberOfPages; ++i) {
			var isSelected = this.state.page == i;
			var shouldRender = i == this.state.page;			
			if(i < this.state.page) {
				if(this.state.page - i < nrRight || i == 0) {
					shouldRender = true;
				} else if(!dottsLeft) {
					dottsLeft = true;
					pages.push(<div key={"d"+i}>...</div>);
				}
			} else if(i > this.state.page) {
				if(i - this.state.page < nrLeft || i == numberOfPages - 1) {
					shouldRender = true;
				} else if(!dottsRight) {
					dottsRight = true;
					pages.push(<div key={"d"+i}>...</div>);
				}
			} else if(i == 0 || i == numberOfPages - 1) {
				shouldRender = true;
			}
			if(shouldRender) {
				pages.push(
					<div 
						className={isSelected ? "selected" : ""}
						tabIndex="0"
						key={i}
						onKeyUp={this.onPageKeyUp.bind(this, i)}
						onPointerUp={this.onPageClicked.bind(this, i)}>
						{i + 1}
					</div>
				);
			}
		}
		return (
			<div className="page_container">
				{pages}
			</div>
		);
	}

	renderItem(item, i) {
		var name = Util.getMetaValue(item.meta, "name", item.name);
		if(name.length == 0) {
			name = item.name;
		}
		return (
			<div
				key={i} 
				tabIndex="0"
				className="choose_file_column"
				onKeyUp={this.onKeyUp.bind(this, item)}
				onPointerUp={this.onFileClicked.bind(this, item)}>
				<div>{name}</div>
				<div>{item.annotations.length}</div>
			</div>
		);
	};

    render() {
		var visited = Context.lastVisited.map(this.renderItem.bind(this));
		var list = this.getPageItems(this.state.page).map(this.renderItem.bind(this));
		return (
			<div className="choose_file_content">
				<div className="button_row">
					<div className="button_list">
						<Button
							onClick={this.onProcessListClicked.bind(this)}
							text="Bearbeta annonser"/>
					</div>
				</div>
				<div className="group choose_file_group">
					{Context.lastVisited.length > 0 &&
						<div>
							<h3>Senast besökta</h3>
							<div className="choose_file_column_headers choose_file_column">
								<div>Namn</div>
								<div>Annoteringar</div>
							</div>
							<div className="choose_file_list">
								{visited}
							</div>
						</div>
					}
					<h3>Filer</h3>
					<div className="choose_file_column_headers choose_file_column">
						<div>Namn</div>
						<div>Annoteringar</div>
					</div>
					<div className="choose_file_list" id="items">
						{list}
					</div>
					{this.renderPageSelector()}
				</div>
				{this.state.isShowingUploadDialog &&
					this.renderUploadListDialog()
				}
			</div>
		);
    }
	
}

export default AnnotateChooseFile