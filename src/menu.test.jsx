import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Menu from "./menu"
import React from "react"

describe("Menu", () => {
  test("renders", () => {
    render(<Menu />);
    expect(screen.getByText(/Skapa ny/)).toBeDefined();
    expect(screen.getByText(/Välj fil/)).toBeDefined();
    expect(screen.getByText(/Dokument/)).toBeDefined();
    expect(screen.getByText(/Inställningar/)).toBeDefined();
    expect(screen.getByText(/Annotation Editor/)).toBeDefined();
    expect(Menu).toBeInstanceOf(Function);
    expect(Menu).toHaveProperty("prototype");
    expect(Menu.prototype).toBeInstanceOf(Object);
    expect(Menu.prototype.constructor).toBe(Menu);
  });
});