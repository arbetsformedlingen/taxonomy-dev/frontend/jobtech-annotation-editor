import React from 'react';
import EventDispatcher from './context/event_dispatcher.jsx';
import Constants from './context/constants.jsx';
import Context from './context/context.jsx';

class Menu extends React.Component { 

	constructor() {
        super();
        // state
        this.state = {
            selected: null,
            items: [{
                id: Constants.MENU_BUTTON_NEW_FILE,
                text: "Skapa ny",
                isEnabled: true,
            }, {
                id: Constants.MENU_BUTTON_SELECT_FILE,
                text: "Välj fil",
                isEnabled: true,
            }, {
                id: Constants.MENU_BUTTON_ANNOTATION,
                text: "Dokument",
                isEnabled: false,
            }, {
                id: Constants.MENU_BUTTON_SETTINGS,
                text: "Inställningar",
                isEnabled: true,
            }],
        };
    }

    componentDidMount() {
        this.onItemClicked(this.state.items[1]);
        EventDispatcher.add(this.onDocumentSet.bind(this), Constants.EVENT_SET_DOCUMENT);
    }

    onKeyUp(item, e) {
        if(e.key == "Enter" && item.isEnabled) {
            this.onItemClicked(item, true);
        }
    }

    onItemClicked(item, isUserEvent) {
        if(isUserEvent) {
            Context.processFiles = [];
        }
        this.setState({selected: item}, () => {
            EventDispatcher.fire(Constants.EVENT_MENU_BUTTON, item.id);
        });
    }

    onDocumentSet(data) {
        this.state.items[2].isEnabled = data != null;
        if(data != null) {
            this.onItemClicked(this.state.items[2]);
        }
    }

    render() {
        var items = this.state.items.map((element, index) => {
            var css = "menu_item" + (this.state.selected == element ? " selected" : "");
            if(element.isEnabled != null && !element.isEnabled) {
                css += " disabled";
            }
            return (
                <div 
                    className={css}
                    key={index}
                    tabIndex={element.isEnabled ? "0" : "-1"}
                    onKeyUp={this.onKeyUp.bind(this, element)}
                    onPointerUp={this.onItemClicked.bind(this, element, true)}>
                    {element.text}
                </div>
            );
        });
        return (
			<div className="menu">
                <div className="menu_item_container">
                    {items}
                </div>
                <div className="logo">
                    Annotation Editor
                </div>
			</div>
        );
    }
	
}

export default Menu