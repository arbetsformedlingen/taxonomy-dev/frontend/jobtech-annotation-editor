import React from 'react';
import Rest from './context/rest.jsx';
import Util from './context/util.jsx';
import Context from './context/context.jsx';
import Button from './controls/button.jsx';

class EditAnnotation extends React.Component { 

	constructor(props) {
        super(props);
        // state
        this.state = {
			searchFor: "",
			searchForResult: null,
			edited: false,
			comment: this.props.annotation.comment ? this.props.annotation.comment : "",
			editingConnetions: [],
        };
		this.firstSelected = null;
		this.latestConnections = [];
		this.load();
		this.getRecommendations(props.annotation["matched-string"]);
    }

	save() {
        try {
            localStorage.setItem("ann_latest_connections", JSON.stringify(this.latestConnections));
        } catch(e) {

        }
    }

    load() {
        var l = localStorage.getItem("ann_latest_connections");
        if(l != null) {
            try {
                this.latestConnections = JSON.parse(l);
				//Make sure latestConnections uses correct name for preferred-label
				for(var i=0; i<this.latestConnections.length; ++i) {
					if(!this.latestConnections[i]["preferred-label"] && this.latestConnections[i].preferredLabel) {
						this.latestConnections[i]["preferred-label"] = this.latestConnections[i].preferredLabel;
						this.latestConnections[i].preferredLabel = null;
					}
				}
            } catch(e) {

            }
        }
    }

	getRecommendations(string, type) {
		if(!type || type.length == 0) {
			type = "";
		}
		var query = {
			array_of_words: [string],
			concept_type: type,
			limit_number: 5
		}
		Rest.postRecommender(query, (data) => {
			var result = data[string];
			if(result) {
				if(Context.lockedToTypes.length > 0) {
					// apply client limitiations
					result = result.filter(item => Context.lockedToTypes.indexOf(item.type) >= 0);
				}
				this.setState({recommendedConnections: result});
			}
		}, (status) => {
			console.log("Error", status);
		});
	}

	addToLatestConnections(concept) {
		// if already present move to top
		var pos = this.latestConnections.findIndex(item => item.id == concept.id);
		if(pos == 0) {
			// already in list at top = done
			return;
		}
		if(pos > 0) {
			// put first in list
			this.latestConnections.unshift(this.latestConnections.splice(pos, 1)[0]);
		} else {
			this.latestConnections.unshift({
				"preferred-label": concept["preferred-label"] ? concept["preferred-label"] : concept.preferred_label,
				type: concept.type,
				id: concept.id
			});
		}
		if(this.latestConnections.length > 5) {
			this.latestConnections.length = 5;
		}
		this.save();
	}

	onSearchForChanged(e) {
		var text = e.target.value;
		this.setState({searchFor: text});
		if(text.length > 1) {
			Rest.autocomplete(text, (data) => {								
				var result = data.filter(item => item.type != "forecast-occupation" && item.type != "isco-level-4" && item.type != "unemployment-type");
				if(Context.lockedToTypes.length > 0) {
					// apply client limitiations
					result = result.filter(item => Context.lockedToTypes.indexOf(item.type) >= 0);
				}
				this.setState({searchForResult: result});
			}, (status) => {
				console.log("Error: " + status)
			});
		} else {
			this.setState({searchForResult: null});
		}
	}

	onChangeAnnotationConnectedCancled(index) {
		var i = this.state.editingConnetions.indexOf(index);
		this.state.editingConnetions.splice(i, 1);
		this.setState({editingConnetions: this.state.editingConnetions});
	}

	onChangeAnnotationConnectedTo(index) {
		this.state.editingConnetions.push(index);
		this.setState({editingConnetions: this.state.editingConnetions});
	}

	onChangeSentiment() {
		this.setState({editSentiment: true});
	}

	onRemoveSentiment() {
		if(this.props.annotation) {
			delete this.props.annotation["sentiment"];
			this.setState({edited: true});
		}
	}

	onSaveNewAnnotationClick() {
		this.props.onAnnotationSaved();
	}

	onDeleteAnnotationClick() {
		this.props.onAnnotationDeleted();
	}

	onSelectAnnotationConcept(concept, index) {
		if(this.props.annotation) {
			if(this.props.annotation.connections == null) {
				// TODO: Remove this
				// ----------------------------------------------------
				this.props.annotation["concept-id"] = concept.id;
				this.props.annotation["preferred-label"] = concept["preferred-label"] ? concept["preferred-label"] : concept.preferred_label;
				this.props.annotation["type"] = concept.type;
				// ----------------------------------------------------
			} else {
				var item = {
					"concept-id": concept.id,
					"preferred-label": concept["preferred-label"] ? concept["preferred-label"] : concept.preferred_label,
					"type": concept.type
				};
				if(index == -1) {
					this.props.annotation.connections.push(item);
				} else {
					this.props.annotation.connections[index] = item;
				}
			}
			this.addToLatestConnections(concept);
		}
		if(index != -1) {
			this.onChangeAnnotationConnectedCancled(index);
		}
		this.setState({
			edited: true,
			searchFor: "",
			searchForResult: null
		});
	}

	onSentimentChanged(e) {
		if(this.props.annotation) {
			this.props.annotation["sentiment"] = e.target.value;
		}
		this.setState({
			edited: true,
			editSentiment: false,
		});
	}

	onTypeChanged(index, e) {		
		if(this.props.annotation) {
			if(this.props.annotation.connections == null) {
				// TODO: Remove this
				// ----------------------------------------------------
				this.props.annotation["concept-id"] = null;
				this.props.annotation["preferred-label"] = null;
				this.props.annotation["type"] = e.target.value;
				// ----------------------------------------------------
			} else {
				this.props.annotation.connections[index] = {
					"concept-id": null,
					"preferred-label": null,
					"type": e.target.value
				};
			}
		}
		if(index != -1) {
			this.onChangeAnnotationConnectedCancled(index);
		}
		this.setState({
			edited: true,
			searchFor: "",
			searchForResult: null
		});
	}

	onCommentChanged(e) {
		this.props.annotation.comment = e.target.value;
		this.setState({
			edited: true,
			comment: e.target.value
		});
	}

	renderDropdown(index) {
		if(this.state.searchForResult) {
			var items = this.state.searchForResult.map((item, i) => {
				var text = Context.hasTypeName(item.type) ? Context.getTypeName(item.type) + " (" + item.type + ")" : item.type;
				return (
					<div
						onPointerUp={this.onSelectAnnotationConcept.bind(this, item, index)} 
						className="dropdown-item" 
						key={i}>
						<div>{text}</div>
						<div>{item["preferred-label"]}</div>
					</div>
				);
			});
			return(
				<div className="dropdown-content">
					{items}
				</div>
			);
		}
	}

	renderOptions() {
		Util.sortByCmp(this.props.annotationTypes, (item) => {
			return Context.hasTypeName(item) ? Context.getTypeName(item) : item;
		}, true);
		var annotationTypes = this.props.annotationTypes.filter(item => item !== "occupation-name" && item !== "skill");
		annotationTypes.unshift("skill");
		annotationTypes.unshift("occupation-name");
		var types = annotationTypes.map((t, i) => {
			var text = Context.hasTypeName(t) ? Context.getTypeName(t) + " (" + t + ")" : t;
			return (
				<option 
					value={t} 
					key={i}>
					{text}
				</option>
			);
		});
		types.unshift(
			<option 
				key={this.props.annotationTypes.lenght + 1} 
				disabled 
				value="">
				-- välj en typ --
			</option>
		);
		return types;
	}

	renderEditConnection(index) {
		return (
			<div key={index}>
				<div>Sök koncept:</div>
				<div className="input_holder">
					<input 
						className="rounded"
						type="text"
						value={this.state.searchFor}
						placeholder="Sök koncept"
						onChange={this.onSearchForChanged.bind(this)}
					/>
				</div>
				<div className="dropdown">
					{this.renderDropdown(index)}
				</div>
				<div>Välj typ:</div>
				<select
					className="rounded"
					value=""
					onChange={this.onTypeChanged.bind(this, index)}>
					{this.renderOptions()}
				</select>
				<div className="button_row">
					<Button
						css="edit"
						onClick={this.onChangeAnnotationConnectedCancled.bind(this, index)}
						text="Avbryt"/>					
				</div>
			</div>
		);
	}
	
	renderDialogConnected() {
		// TODO: Remove this
		// NOTE: this block is temporary until the backend implements
		//		 support for multiple connections
		// ----------------------------------------------------
		var tmp = [];
		if(this.props.annotation.connections == null) {
			tmp.push({
				"preferred-label" : this.props.annotation["preferred-label"],
				"type" : this.props.annotation.type,
				"concept-id" : this.props.annotation["concept-id"]
			});
		}
		// ----------------------------------------------------
		var connections = tmp.map((element, index) => {
			if(this.state.editingConnetions.indexOf(index) >= 0) {
				return this.renderEditConnection(index);
			}
			var type = element.type;
			var typeText = Context.hasTypeName(type) ? Context.getTypeName(type) + " (" + type + ")" : type;
			return (
				<div key={index}>
					{this.renderField("Typ", typeText)}
					{element["concept-id"] &&
						this.renderField("Konceptid", element["concept-id"])
					}
					{element["preferred-label"] &&
						this.renderField("Konceptname", element["preferred-label"])
					}
					<div className="button_row">
						<Button
							css="edit"
							onClick={this.onChangeAnnotationConnectedTo.bind(this, index)}
							text="Ändra"/>					
					</div>
				</div>
			);
		});
		return (
			<div className="dialog_group">
				<div className="title">Kopplingar</div>
				<div className="connection_group">
					{connections}
				</div>
			</div>
		);

	}

	renderSentimentDropDown() {
		var sentiments = Context.sentimentLocalization.map((item, i) => {
			return (
				<option 
					value={item.id} 
					key={i}>
					{item["swedish-label"]}
				</option>
			);
		});
		sentiments.unshift(
			<option 
				key={this.props.annotationTypes.lenght + 1} 
				disabled 
				value="">
				-- välj ett sentiment --
			</option>
		);		
		
		return (
			<select
				className="rounded"
				value=""
				onChange={this.onSentimentChanged.bind(this)}>
				{sentiments}
			</select>
		);
	}

	renderSentiment() {
		return (
			<div className="dialog_group">
				<div className="title">Sentiment</div>
				{!this.state.editSentiment &&
					this.renderField(Context.getSentimentLocalized(this.props.annotation["sentiment"]), 
						<div className='button_row'>
						{this.props.annotation["sentiment"] &&
							<Button
								css="edit"
								onClick={this.onRemoveSentiment.bind(this)}
								text="Rensa"/>
						}
						<Button
							css="edit"
							onClick={this.onChangeSentiment.bind(this)}
							text="Ändra"/>
						</div>)
				}
				{this.state.editSentiment &&
					this.renderSentimentDropDown()
				}
			</div>
		);
	}

	renderField(name, val) {
		return (
			<div className="annotation_field">
				<div>{name}</div>
				<div>{val}</div>
			</div>
		);
	};

	renderConnections(list) {
		if(list && list.length > 0) {	
			var connections = list.map((concept, i) => {
				var title = concept["preferred-label"] ? concept["preferred-label"] : concept.preferred_label;
				return (
					<div 
						className="dialog_group latest_connections"
						key={i}
						onPointerUp={this.onSelectAnnotationConcept.bind(this, concept, -1)}>
						<div title={title}>{title}</div>
						<div>{concept.type}</div>					
					</div>
				);
			});
			return connections;
		}
	}

    render() {
        return (
			<div>
				<div>
					<div className="title">Edit</div>
					<div className="dialog_content">
						<div className="dialog_group">
							<div className="title">Info</div>
							{this.renderField("Id", this.props.annotation["annotation-id"])}
							{this.renderField("Matched string", this.props.annotation["matched-string"])}
						</div>
						{this.renderDialogConnected()}
						{this.renderSentiment()}					
						<div className="dialog_group">
							<div className="title">Kommentar</div>
							<textarea 
								value={this.state.comment}
								onChange={this.onCommentChanged.bind(this)}/>
						</div>
					</div>
					<div className="button_row">
						{this.props.newConnection == true &&
						<Button
							css="save"
							isEnabled={this.props.annotation.type ? true : false}
							onClick={this.onSaveNewAnnotationClick.bind(this)}
							text="Spara"/>
						}
						{this.props.newConnection != true &&
						<Button
							css="remove"
							onClick={this.onDeleteAnnotationClick.bind(this)}
							text="Ta bort"/>
						}
						<Button
							onClick={this.props.onCloseDialog.bind(this, this.state.edited)}
							text={this.props.newConnection == true ? "Avbryt" : "Stäng"}/>
					</div>
				</div>
				{(this.latestConnections.length > 0 || this.state.recommendedConnections > 0) &&
					<div>
						<div className="title">Kopplingsförslag</div>
						{this.renderConnections(this.latestConnections)}
						<hr/>
						{this.renderConnections(this.state.recommendedConnections)}
					</div>
				}
			</div>
        );
    }
	
}

export default EditAnnotation