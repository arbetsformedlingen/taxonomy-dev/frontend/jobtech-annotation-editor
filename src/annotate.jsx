import React from 'react';
import Rest from './context/rest.jsx';
import Context from './context/context.jsx';
import Constants from './context/constants.jsx';
import Util from './context/util.jsx';
import Keybindings from './context/key_bindings.jsx';
import Button from './controls/button.jsx';
import EditCreateMeta from './edit_create_meta.jsx';
import EditAnnotation from './edit_annotation.jsx';

class Annotate extends React.Component { 

	constructor(props) {
        super(props);
        // state
        this.state = {
			data: this.props.data,
			selected: null,
			selectedTypes: [],
			selectedText: null,
			selectedId: null,
			filter: null,
			isCreatingMeta: false,
			isGroupingOnType: false,
			edited: this.props.edited,
			metaEdited: false,
			errors: [],
        };
		this.types = Context.types.filter(item => item != "forecast-occupation" && item != "isco-level-4" && item != "unemployment-type");
		this.boundGlobalInput = this.onGlobalInput.bind(this);
		this.boundSelectionChange = this.onSelectionChange.bind(this);
		this.boundUserLeaveSite = this.onUserLeaveSite.bind(this);
		this.init();
    }

    componentDidMount() {
		document.addEventListener("selectionchange", this.boundSelectionChange);
		window.addEventListener('beforeunload', this.boundUserLeaveSite);
		document.addEventListener('keydown', this.boundGlobalInput);
    }

    componentWillUnmount() {
		document.removeEventListener("selectionChange", this.boundSelectionChange);
		window.removeEventListener('beforeunload', this.boundUserLeaveSite);
		document.removeEventListener('keydown', this.boundGlobalInput);
    }

	UNSAFE_componentWillReceiveProps(props) {
        var state = {
			data: props.data,
			selected: null,
			selectedTypes: [],
			selectedText: null,
			selectedId: null,
			filter: null,
			isCreatingMeta: false,
			isGroupingOnType: false,
			edited: props.edited,
			metaEdited: false,
			errors: [],
        };
		this.init();
		this.setState(state);
	}

	init() {
		this.firstSelected = null;
		if(this.state.data.meta == undefined) {
			this.state.data.meta = {
				name: "",
				type: "",
				description: ""
			};
		} else if(Array.isArray(this.state.data.meta)) {
			// convert old array meta format to new object format
			var tmp = {};
			for(var i=0; i<this.state.data.meta.length; ++i) {
				var item = this.state.data.meta[i];
				var keys = Object.keys(item);
				tmp[keys[0]] = item[keys[0]];
			}
			this.state.data.meta = tmp;
		}
		// make sure name is present in meta
		if(this.state.data.meta.name == undefined) {
			this.state.data.meta.name = "";
		}
		this.origMeta = JSON.stringify(this.state.data.meta);
	}

	updateMetaEdited() {
		this.setState({metaEdited: this.origMeta != JSON.stringify(this.state.data.meta)});
	}

	getColorFor(type) {		
		var i = this.types.indexOf(type);
		if(i >= 0) {
			return Constants.ANNOTATION_COLORS[i];
		}
		return Constants.ANNOTATION_COLORS[0];
	}

	onGlobalInput(e) {
        if(Keybindings.onKeyBinding(e, Keybindings.BINDING_NEW_ANNOTATION) && this.state.selectedText) {
			this.onCreateAnnotationClicked(this.state.selectedText);
		} else if(Keybindings.onKeyBinding(e, Keybindings.BINDING_NEW_META)) {
			this.onCreateMetaClicked();
		} else if(Keybindings.onKeyBinding(e, Keybindings.BINDING_SAVE_DOCUMENT) && this.state.edited) {
			this.onSaveClicked();
		} else if(this.state.selected || this.state.isCreatingMeta) {
			if(e.key == "Escape") {
				this.setState({
					selected: null,
					isCreatingMeta: false
				});
			}
		}
    }

	onUserLeaveSite(e) {
		if(this.state.edited) {
			e.preventDefault();
			e.returnValue = '';
		}
	}

	onNotifyChanged(status) {
		if(this.props.changedCallback) {
			this.props.changedCallback(status);
		}
	}

	onFilterChanged(e) {
		this.setState({filter: e.target.value});
	}

	onGroupOnTypeChanged(e) {
		this.setState({
			selectedId: null,
			isGroupingOnType: e.target.checked,
		});
	}

	onSelectionChange(e) {		
		var selection = window.getSelection();		
		if(selection.anchorNode != null && selection.anchorNode == selection.focusNode) {
			var element = selection.anchorNode.parentElement;
			if(element.id && element.id.startsWith("a_")) {
				if(selection.anchorOffset != selection.focusOffset) {
					var id = element.id.substring(2);
					var annotations = this.state.data.annotations.filter((a) => {
						return a["annotation-id"] == id;
					});					
					if(annotations.length > 0 || id == "first") {						
						var from = selection.anchorOffset;
						var to = selection.focusOffset;
						if(annotations.length > 0) {
							from += annotations[0]["end-position"];
							to += annotations[0]["end-position"];
						}
						if(from > to) {
							var tmp = from;
							from = to;
							to = tmp;
						}
						var text = selection.anchorNode.nodeValue.substring(selection.anchorOffset, selection.focusOffset);
						for(var pos=0; pos < text.length; ++pos) {
							if(text[pos] != ' ' && text[pos] != '\t' && text[pos] != '\n') {
								if(pos > 0) {
									text = text.substring(pos);
									from += pos;									
								}
								break;
							}
						}
						for(var pos=0; pos < text.length; ++pos) {
							var c = text[text.length - 1 - pos];
							if(c != ' ' && c != '\t' && c != '\n') {
								text = text.substring(0, text.length - pos);
								to -= pos;								
								break;
							}
						}
						if(text.length > 0) {
							this.setState({selectedText: {
								text: text,
								from: from,
								to: to,
							}});
							return;
						}
					}
				}
				if(this.state.selectedText) {
					this.setState({selectedText: null});
				}
			} else if(element.id && element.id.startsWith("annotation_")) {
				if(this.state.selectedText) {
					this.setState({selectedText: null});
				}
			}
		} else if(selection.anchorNode != null && this.state.selectedText) {
			this.setState({selectedText: null});
		}
	}

	onCreateAnnotationClicked(selection) {
		var nextId = ()=>{
			var id = 0;
			this.state.data.annotations.forEach((a) => {
				if(a["annotation-id"] > id) {
					id = a["annotation-id"];
				}				
			});
			return id + 1;
		};
		if(selection) {
			var text = this.state.data.text.substring(selection.from, selection.to);
			var id = nextId();
			this.setState({selected: {
					"annotation-id": id,
					"concept-id": null,
					"end-position": selection.to,
					"matched-string": text,
					"preferred-label": null,
					"start-position": selection.from,
					type: null,
				},
				editConnection: true,
				newConnection: true,
				selectedText: null,});
		}		
	}

	onSaveNewAnnotationClick() {
		this.onNotifyChanged(true);
		this.state.data.annotations.push(this.state.selected);
		this.setState({
			selected: null,
			edited: true,
		});
	}

	onDeleteAnnotationClick() {
		this.onNotifyChanged(true);
		var i = this.state.data.annotations.indexOf(this.state.selected);
		if(i >= 0) {
			this.state.data.annotations.splice(i, 1);
		}
		this.setState({
			selected: null,
			edited: true,
		});
	}

	onSelectAnnotationClick(annotation) {
		this.setState({
			selected: annotation,
			newConnection: false,
		});
	}

	onToggleTypeSelectedClicked(type) {
		var pos = this.state.selectedTypes.findIndex((a)=>{return a == type;});
		if(pos >= 0) {
			this.state.selectedTypes.splice(pos, 1);
		} else {
			this.state.selectedTypes.push(type);
		}
		this.forceUpdate(() => {
			setTimeout(()=>{
				if(this.firstSelected) {
					var element = document.getElementById("annotation_" + this.firstSelected["annotation-id"]);				
					if(element) {
						element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
					}
				}
			}, 500);
		});
	}

	onAnnotationKeyUp(annotation, e) {
		if(e.key == "Enter") {
			this.onAnnotationClicked(annotation);
		} else if(Keybindings.onKeyBinding(e, Keybindings.BINDING_OPEN_ANNOTATION)) {
			this.onSelectAnnotationClick(annotation);
		}
	}

	onAnnotationClicked(annotation) {
		this.setState({
			selectedId: this.state.selectedId == annotation["annotation-id"] ? null : annotation["annotation-id"],
		}, () => {
			if(this.state.selectedId != null) {
				var element = document.getElementById("annotation_" + this.state.selectedId);
				if(element) {
					element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
				}
			}
		});	
	}

	onCreateMetaClicked() {
		this.setState({isCreatingMeta: true});
		//this.state.data.meta[""] = "";
		//this.updateMetaEdited();
	}

	onMetaValueChanged(key, e) {
		this.state.data.meta[key] = e.target.value;
		this.updateMetaEdited();
	}

	onMetaValueKeyUp(key, e) {
		if(e.key == "Enter") {
			this.onMetaValueRemoveClicked(key);
		}
	}

	onMetaValueRemoveClicked(key) {
		delete this.state.data.meta[key];
		this.updateMetaEdited();
	}

	onAnnotationSaved() {
		this.onSaveNewAnnotationClick();
	}

	onAnnotationChanged() {
		this.onNotifyChanged(true);
		this.setState({
			selected: null,
			edited: true,
		});
	}

	onAnnotationDeleted() {
		this.onDeleteAnnotationClick();
	}

	onHideDialog(edited) {
		this.onNotifyChanged(edited);
		this.setState({
			selected: null,
			edited: edited,
		});
	}

	copyToClipboard(data) {
		var dump = () => {
			this.onAddError("Misslyckades kopiera data till urklipp, datadump finns i loggen");
			console.log("failed to copy data to clipboard");
			console.log(data);
		};
		var fallbackCopyTextToClipboard = (text) => {
			var textArea = document.createElement("textarea");
			textArea.value = text;
			textArea.style.top = "0";
			textArea.style.left = "0";
			textArea.style.position = "fixed";
			document.body.appendChild(textArea);
			textArea.focus();
			textArea.select();
			try {
				if(!document.execCommand('copy')) {
					dump();
				}
			} catch (err) {
				dump();
			}
			document.body.removeChild(textArea);
		};
		if (!navigator.clipboard) {
            fallbackCopyTextToClipboard(data);
        } else {
			try {
				navigator.clipboardx.writeText(data).then(() => {}, () => {
					dump();
				});
			} catch(err) {
				dump();
			}
        }
	}
	
    showSaveIndicator() {
        var indicator = document.getElementById("save_indicator");
        indicator.classList.add("save_enter_margin");
    }

    hideSaveIndicator() {
        var indicator = document.getElementById("save_indicator");
        indicator.classList.remove("save_enter_margin");
    }

	onNextClicked() {
		if(this.state.edited || this.state.metaEdited) {
			var r = window.confirm("Det finns osparade förändringar, vill du spara?");
			if(r == true) {
				this.onSaveClicked(this.onNextClicked.bind());
			} else {
				Context.processIndex++;
				Context.visitFile(Context.processFiles[Context.processIndex]);
			}
		} else {
			Context.processIndex++;
			Context.visitFile(Context.processFiles[Context.processIndex]);
		}
	}

	onSaveClicked(onSaved) {
		this.showSaveIndicator();
		Rest.saveAnnotatedText(this.state.data, (data) => {
			this.hideSaveIndicator();
			this.onNotifyChanged(false);
			if(this.props.isNew) {
				Context.addLastVisited(this.state.data);
			}
			this.origMeta = JSON.stringify(this.state.data.meta);
			this.setState({
				edited: false,
				metaEdited: false,
			}, () => {
				if(onSaved) {
					onSaved();
				}
			});
		}, (status) => {
			this.hideSaveIndicator();
			this.onAddError("Misslyckades att spara dokument (felkod: " + status + ")");
			console.log("failed to save document", status);
		});
	}

	onCopyToClipboardClicked() {
		this.copyToClipboard(JSON.stringify(this.state.data));
	}

	buildText() {
		var isSelected = (item) => {
			return this.state.selectedId == null || this.state.selectedId == item["annotation-id"];
		};
		this.firstSelected = null;
		var res = [];
		var annotations = this.state.data.annotations.sort((a, b) => { return a["start-position"] - b["start-position"]; });
		var text = this.state.data.text;
		var key = 0;
		var lastEnd = 0;
		var lastId = "first";
		for(var i=0; i<annotations.length; ++i) {
			var annotation = annotations[i];
			if(lastEnd <= annotation["start-position"]) {
				var title = annotation.type;
				if(annotation["preferred-label"]) {
					title += ", " + annotation["preferred-label"];
				}
				var isConceptBound = annotation["concept-id"] != null;
				var styleColor = this.getColorFor(annotation.type);
				var style = isConceptBound ? {background: styleColor} : {border: "2px solid " + styleColor};
				var css = "annotation_tag annotation_no_highlight";
				if(isSelected(annotation)) {
					css = "annotation_tag";
					if(this.firstSelected == null) {
						this.firstSelected = annotation;
					}
				}
				if(lastEnd < annotation["start-position"]) {
					res.push(
						<span 
							key={key++}
							id={"a_" + lastId}>
							{text.substring(lastEnd, annotation["start-position"])}
						</span>
					);
				}
				res.push(
					<span 
						key={key++} 
						className={css}
						style={style}
						id={"annotation_" + annotation["annotation-id"]}
						onPointerUp={this.onSelectAnnotationClick.bind(this, annotation)}
						title={title}>
						{text.substring(annotation["start-position"], annotation["end-position"])}
					</span>
				);
			}
			lastEnd = annotation["end-position"];
			lastId = annotation["annotation-id"];
		}
		if(lastEnd < text.length) {
			res.push(
				<span 
					key={key++} 
					id={"a_" + lastId}>
					{text.substring(lastEnd)}
				</span>
			);
		}
		return res;
	}

    onAddError(message) {
        var error = {
            message: message,
            active: true,
        };
        this.state.errors.push(error);
        this.setState({errors: this.state.errors});
        // set timeout to inactivate the element
        setTimeout(() => {
            error.active = false;
            this.setState({errors: this.state.errors});
            // set timeout check if we should clear the list
            setTimeout(() => {
                var keep = this.state.errors.find((x) => {
                    return x.active;
                }) != null;
                if(!keep) {
                    //this.setState({errors: []});
                }
            }, 500);
        }, 10000);
    }

	renderText() {
		return (
			<div id="annotation_text" className="annotation_text">
				{this.buildText()}
			</div>
		);
	}

	renderAnnotationFilter() {
		return (
			<div className="annotation_filter">
				<div>
					<label htmlFor="annotation_filter_checkbox">Gruppera efter typ</label>
					<input 
						id="annotation_filter_checkbox"
						type="checkbox"
						value={this.state.isGroupingOnType}
						onChange={this.onGroupOnTypeChanged.bind(this)}/>
				</div>
				<input 
					placeholder="Filter..." 
					value={this.state.filter == null ? "" : this.state.filter}
					onChange={this.onFilterChanged.bind(this)}/>
			</div>
		);
	}

	renderTags() {
		var isSelected = (item) => {
			return this.state.selectedId == item["annotation-id"];
		}
		var renderItem = (item, index) => {
			var style = {background: this.getColorFor(item.type)};
			var css = isSelected(item) ? "annotation_type selected" : "annotation_type";
			var isConceptBound = item["concept-id"] != null;	
			if(!isConceptBound) {
				style = {
					border: "2px solid " + style.background,
					background: "#fff"
				};
			}
			return (
				<div 
					key={index} 
					className={css}
					style={style}
					tabIndex="0"
					onKeyUp={this.onAnnotationKeyUp.bind(this, item)}
					onPointerUp={this.onAnnotationClicked.bind(this, item)}>
					<span title={item["matched-string"]}>
						{item["matched-string"]}
					</span>
				</div>
			);
		};
		// show all items
		var items = this.state.data.annotations.filter((item) => {
			return this.state.filter == null || item["matched-string"].indexOf(this.state.filter) != -1;
		});
		var listItems = [];
		if(this.state.isGroupingOnType) {
			for(var i=0; i<items.length; ++i) {
				var item = items[i];
				var entry = listItems.find((e) => {
					return e.type == item.type;
				});
				if(entry == null) {
					entry = {
						type: item.type,
						items: [],
					};
					listItems.push(entry);
				}
				entry.items.push(item);
			}
			listItems = listItems.map((item, index) => {
				var subItmes = item.items.map((subItem, subIndex) => {
					return renderItem(subItem, subIndex);
				});
				var style = {background: this.getColorFor(item.type)};
				var s = {
					marginLeft: 10,
				};
				var text = Context.hasTypeName(item.type) ? Context.getTypeName(item.type) + " (" + item.type + ")" : item.type;
				return (
					<div key={index}>
						<div className="annotation_type_category" style={style}>
							<span title={text}>{text}</span>
						</div>
						<div className="annotation_list" style={s}>{subItmes}</div>
					</div>
				);
			});
		} else {
			listItems = items.map((item, index) => {
				return renderItem(item, index);
			});
		}
		return (
			<div className="annotation_types group">
				<h3>Annoteringar</h3>
				{this.renderAnnotationFilter()}
				<div className="annotation_list">{listItems}</div>
			</div>
		);
	}

	renderMeta() {
		var index = 0;
		var items = [];
		for(var key in this.state.data.meta) {
			var value = this.state.data.meta[key];
			items.push((
				<div
					key={index}
					className="annotation_meta">
					<input  
						value={key}
						disabled={true}/>
					<input 
						placeholder="Value" 
						value={value == null ? "" : value}
						disabled={typeof(value) === 'object'}
						onChange={this.onMetaValueChanged.bind(this, key)}/>
					<div 
						className="meta_remove"
						tabIndex="0"
						onKeyUp={this.onMetaValueKeyUp.bind(this, key)}
						onPointerUp={this.onMetaValueRemoveClicked.bind(this, key)}/>
				</div>
			));
			index++;
		}
		return (
			<div className="annotation_metas group">
				<h3>Meta</h3>
				<div className='annotation_meta_list'>
					{items}
				</div>			
			</div>
		);
	}

	renderAnnotateContent() {
		return (
			<div className="annotation_container">
				<div className="button_row">
					<div className="button_list">
						<Button
							isEnabled={this.state.selectedText ? true : false}
							onClick={this.onCreateAnnotationClicked.bind(this, this.state.selectedText)}
							text="Ny annotering"/>
						<Button						
							onClick={this.onCreateMetaClicked.bind(this)}
							text="Ny meta"/>
					</div>
					{Context.processFiles.length > 0 &&
						<div className="button_list">
							<div className="annotation_process_label">
								Bearbetar ({Context.processIndex + 1}/{Context.processFiles.length})
							</div>
							{Context.processIndex < Context.processFiles.length - 1 &&
								<Button
									onClick={this.onNextClicked.bind(this)}
									text="Nästa"/>
							}
						</div>
					}
					<div className="button_list">
						<Button
							title="Kopiera till urklipp"
							onClick={this.onCopyToClipboardClicked.bind(this)}
							text={Constants.ICON_COPY_CLIPBOARD}/>
						<Button
							css="save"
							isEnabled={this.state.edited || this.state.metaEdited ? true : false}
							onClick={this.onSaveClicked.bind(this)}
							text="Spara"/>
					</div>
				</div>
				<div className="annotation_main">
					<div className="annotation_result group">
						<h3>Dokument</h3>
						{this.renderText()}
					</div>
					<div className="annotation_side">
						{this.renderTags()}
						{this.renderMeta()}
					</div>
				</div>
			</div>
		);
	}

	renderNewNameDialog() {
		return (
			<div className="edit_overlay">
				<div className="edit_dialog">
					<EditCreateMeta
						meta={this.state.data.meta}
						onSave={this.updateMetaEdited.bind(this)}
						onCloseDialog={() => {
							this.setState({isCreatingMeta: false})
						}}/>
				</div>
			</div>
		);
	}

	renderDialog() {
		return (
			<div className="edit_overlay">
				<div className="edit_dialog">
					<EditAnnotation
						newConnection={this.state.newConnection == true}
						annotation={this.state.selected}
						annotationTypes={this.types}						
						onAnnotationSaved={this.onAnnotationSaved.bind(this)}
						onAnnotationChanged={this.onAnnotationChanged.bind(this)}
						onAnnotationDeleted={this.onAnnotationDeleted.bind(this)}
						onCloseDialog={this.onHideDialog.bind(this)}/>
				</div>
			</div>
		);
	}

    renderSaveIndicator() {
        return (
            <div className="save_indicator_content">
                <div
                    id="save_indicator" 
                    className="save_indicator font">
                    <div className="loading_indicator"/>
                    <div>
                        Sparar...
                    </div>
                </div>
            </div>
        );
    }

    renderErrors() {
        if(this.state.errors.length) {
            var items = this.state.errors.map((element, i) => {
                return (
                    <div
                        className={element.active ? "" : "app_error_inactive"} 
                        key={i}>
                        {element.message}
                    </div>
                );
            });
            return (
                <div className="app_error_content">
                    <div className="app_error_list font">
                        {items}
                    </div>
                </div>
            );
        }
    }

    render() {
        return (
			<div className="content">
				{this.renderAnnotateContent()}
				{this.state.selected &&
					this.renderDialog()
				}
				{this.state.isCreatingMeta &&
					this.renderNewNameDialog()
				}
                {this.renderErrors()}
                {this.renderSaveIndicator()}
			</div>
        );
    }
	
}

export default Annotate