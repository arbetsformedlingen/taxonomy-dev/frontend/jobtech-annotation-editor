import React from 'react';

class Loader extends React.Component { 

    constructor() {
        super();
    }

    render() {
        var text = this.props.text ? this.props.text : "Laddar...";
        return (
            <div className="spin_loader_group font">
                <div className="spin_loader"/>
                <div>{text}</div>
            </div>
        );
    }
	
}

export default Loader;