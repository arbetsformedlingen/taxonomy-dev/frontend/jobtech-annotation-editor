import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import AnnotateChooseFile from "./annotate_choose_file"
import React from "react"

describe("AnnotateChooseFile", () => {
  test("renders", () => {
    render(<AnnotateChooseFile />);
    expect(screen.getByText(/Bearbeta annonser/)).toBeDefined();
    expect(screen.getByText(/Filer/)).toBeDefined();
    expect(screen.getByText(/Annoteringar/)).toBeDefined();
    expect(screen.getByText(/Namn/)).toBeDefined();
    expect(screen.getAllByRole("heading", { hidden: true })).toBeDefined();
    expect(AnnotateChooseFile).toBeInstanceOf(Function);
    expect(AnnotateChooseFile).toHaveProperty("prototype");
    expect(AnnotateChooseFile.prototype).toBeInstanceOf(Object);
    expect(AnnotateChooseFile.prototype.constructor).toBe(AnnotateChooseFile);
  });
});