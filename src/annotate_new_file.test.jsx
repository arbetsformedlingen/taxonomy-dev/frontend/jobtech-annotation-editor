import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import AnnotateNewFile from "./annotate_new_file"
import React from "react"

describe("AnnotateNewFile", () => {
  test("renders", () => {
    render(<AnnotateNewFile />);
    expect(screen.getByText(/Skapa ny/)).toBeDefined();
    expect(screen.getByText(/Från text/)).toBeDefined();
    expect(screen.getByText(/Från platsannons/)).toBeDefined();
    expect(screen.getByText(/Dra och släpp här/)).toBeDefined();
    expect(screen.getAllByRole("heading", { hidden: true })).toBeDefined();
    expect(AnnotateNewFile).toBeInstanceOf(Function);
    expect(AnnotateNewFile).toHaveProperty("prototype");
    expect(AnnotateNewFile.prototype).toBeInstanceOf(Object);
    expect(AnnotateNewFile.prototype.constructor).toBe(AnnotateNewFile);
  });
});