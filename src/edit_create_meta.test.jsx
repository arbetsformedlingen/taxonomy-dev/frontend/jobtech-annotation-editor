import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import EditCreateMeta from "./edit_create_meta"
import React from "react"

describe("EditCreateMeta", () => {
  test("renders", () => {
    render(<EditCreateMeta />);
    expect(screen.getByText(/Ny meta/)).toBeDefined();
    expect(screen.getByText(/Spara/)).toBeDefined();
    expect(screen.getByText(/Stäng/)).toBeDefined();
    expect(screen.getAllByRole("textbox", { hidden: true })).toBeDefined();
    expect(EditCreateMeta).toBeInstanceOf(Function);
    expect(EditCreateMeta).toHaveProperty("prototype");
    expect(EditCreateMeta.prototype).toBeInstanceOf(Object);
    expect(EditCreateMeta.prototype.constructor).toBe(EditCreateMeta);
  });
});