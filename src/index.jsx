import React from 'react';
import ReactDOM from 'react-dom';
import Menu from './menu.jsx';
import EventDispatcher from './context/event_dispatcher.jsx';
import Constants from './context/constants.jsx';
import Context from './context/context.jsx';
import Rest from './context/rest.jsx';
import Annotate from './annotate.jsx';
import AnnotateNewFile from './annotate_new_file.jsx';
import AnnotateChooseFile from './annotate_choose_file.jsx';
import AnnotateSettings from './annotate_settings.jsx';

class Index extends React.Component { 

	constructor() {
        super();
        // state
        this.state = {
			contentId: null,
			annotationData: null,
			annotationDataIsNew: false,
			annotationIsNewFile: false,
		};
		this.hasUnsavedChanges = false;
		this.onMenuButtonCallback = this.onMenuButton.bind(this);
		this.onAnnotationCallback = this.onAnnotation.bind(this);
	}
	
	componentDidMount() {
		EventDispatcher.add(this.onMenuButtonCallback, Constants.EVENT_MENU_BUTTON);
		EventDispatcher.add(this.onAnnotationCallback, Constants.EVENT_ANNOTATIONS);
		Context.onInit();
	}

	onDataModified(status) {
		this.hasUnsavedChanges = status;
	}

	onMenuButton(id) {
		this.setState({contentId: id});
	}

	onAnnotation(data) {
		this.setState({
			annotationData: data.data, 
			annotationDataIsNew: data.edited,
			annotationIsNewFile: data.isNew,
		}, () => {
			EventDispatcher.fire(Constants.EVENT_SET_DOCUMENT, data);
		});
	}

	renderContent(id) {
		if(id != Constants.MENU_BUTTON_ANNOTATION && this.hasUnsavedChanges) {
			var r = window.confirm("Det finns osparade förändringar, vill du spara?");
			if(r == true) {
				Rest.saveAnnotatedText(this.state.annotationData, (data) => {
				}, (status) => {
					console.log("failed to save document", status)
				});
			}
			EventDispatcher.fire(Constants.EVENT_SET_DOCUMENT, null);
			this.hasUnsavedChanges = false;
		}
		if(id == Constants.MENU_BUTTON_NEW_FILE) {
			return <AnnotateNewFile/>;
		} else if(id == Constants.MENU_BUTTON_SELECT_FILE) {
			return <AnnotateChooseFile/>;
		} else if(id == Constants.MENU_BUTTON_ANNOTATION) {
			return <Annotate 
				data={this.state.annotationData}
				edited={this.state.annotationDataIsNew}
				isNew={this.state.annotationIsNewFile}
				changedCallback={this.onDataModified.bind(this)}/>
		} else if(id == Constants.MENU_BUTTON_SETTINGS) {
			return <AnnotateSettings/>;
		}
	}

    render() {
        return (
            <div className="main">
				<Menu/>
				<div className="content_container">
					{this.renderContent(this.state.contentId)}
				</div>
			</div>
        );
    }
	
}

ReactDOM.render(<Index/>, document.getElementById('content'));