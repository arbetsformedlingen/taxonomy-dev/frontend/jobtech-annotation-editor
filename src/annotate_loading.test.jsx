import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import AnnotateLoading from "./annotate_loading"
import React from "react"

describe("AnnotateLoading", () => {
  test("renders", () => {
    render(<AnnotateLoading />);
    expect(screen.getByText(/Laddar.../)).toBeDefined();
    expect(AnnotateLoading).toBeInstanceOf(Function);
    expect(AnnotateLoading).toHaveProperty("prototype");
    expect(AnnotateLoading.prototype).toBeInstanceOf(Object);
    expect(AnnotateLoading.prototype.constructor).toBe(AnnotateLoading);
  });
});