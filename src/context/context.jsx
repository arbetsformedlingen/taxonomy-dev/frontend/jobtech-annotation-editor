
import Rest from './rest.jsx';
import Constants from './constants.jsx';
import EventDispatcher from './event_dispatcher.jsx';
import Keybindings from './key_bindings.jsx';

class Context { 

    constructor() {
        this.files = [];
        this.lastVisited = [];
        this.typeNames = [];
        this.isFetchingFiles = false;
        this.onFetchingFilesComplete = null;
        this.onFetchingFileDataUpdated = null;
        this.sentimentLocalization = [];
        this.lockedToTypes = [];
        this.types = [];
        // process mode properties
        this.processIndex = 0;
        this.processFiles = [];
    }

    onInit() {
        Keybindings.load();
        Rest.getTypes((data) => {
			this.types = data;
		}, (status) => {
		});
        /*Rest.getTypesLocalization((data) => {
            //this.typeNames = data;
        });*/
        Rest.getSentimentTypes((data) => {
            this.sentimentLocalization = data;
        });
        // allow client to limit what types can be used for annotations
        var lockedToTypes = this.getUriArg("lockedToTypes");
        if(lockedToTypes != null) {
            this.lockedToTypes = decodeURIComponent(lockedToTypes).split(",");
            for(var i=0; i<this.lockedToTypes.length; ++i) {
                // highly unlickley but just to be sure, if a type is numeric we enforce string type
                this.lockedToTypes[i] = ("" + this.lockedToTypes[i]).trim();
            }
        }
        this.fetchFiles(0);
    }

    hasTypeName(id) {
        return this.typeNames.find((x) => { return x.id == id; }) != null;
    }

    getTypeName(id) {
        var type = this.typeNames.find((x) => { return x.id == id; });
        return type ? type.label_sv : null;
    }

    getSentimentLocalized(sentiment) {
        var localization = this.sentimentLocalization.find((x) => { return x.id == sentiment});
        return localization ? localization["swedish-label"] : sentiment;
    }

    getTypes() {
        if(this.lockedToTypes.length > 0) {
            return this.lockedToTypes.join(",");
        }
        // TODO: return null when api supports it
        return this.types.join(",");
    }

    getFile(name) {
        return this.files.find((element) => {
            return element.name == name;
        });
    }

	fetchFiles(page) {
        this.isFetchingFiles = true;
		Rest.getFilesFromGitLab(page, (data) => {
			this.files.push(...data.filter((item) => { return item.name.indexOf(".json") > 0; }));
			if(data.length == 100) {
				this.fetchFiles(page + 1);
			} else {
                this.files = this.files.map((element) => {
                    return {
                        name: element.name,
                        annotations: [],
                        meta: null,
                        refreshDate: Date.now(),
                    };
                });
                var lastVisited = localStorage.getItem("ann_last_visited");
                if(lastVisited != null) {
                    lastVisited = JSON.parse(lastVisited);
                    for(var i=0; i<lastVisited.length; ++i) {
                        var item = this.files.find((element) => {
                            return element.name == lastVisited[i];
                        });
                        if(item) {
                            this.lastVisited.push(item);
                        }
                    }
                }
                this.isFetchingFiles = false;
                if(this.onFetchingFilesComplete) {
                    this.onFetchingFilesComplete();
                }
			}
		}, (status) => {
			console.log(status);
		});
	}

    async fetchFileData(files) {
        var now = Date.now();
        var targets = files.filter((item) => {
            // refresh items each hour
            return item.annotations.length == 0 || (now - item.refreshDate > 1000 * 60 * 60);
        });
        for(var i=0; i<targets.length; ++i) {
            var data = [];
            for(var j=0; j<4 && i<targets.length; ++j, ++i) {
                data.push(Rest.getFileFromGitLabPromise(targets[i].name));
            }
            for(var j=0; j<data.length; ++j) {
                data[j] = await data[j];
            }
            for(var j=0; j<data.length; ++j) {
                var file = targets[i - data.length + j];
                file.meta = data[j].meta;
                file.annotations = data[j].annotations;
                file.refreshDate = now;
            }
            if(this.onFetchingFileDataUpdated != null) {
                this.onFetchingFileDataUpdated();
            }
        }
    }

    addLastVisited(item) {
        if(item.name == null && item.sha1) {
            item.name = item.sha1 + ".json";
        }
        if(item.name) {
            // check for duplicate
            if(this.lastVisited.find((element) => { return element.name == item.name; }) == null) {
                // add new entry
                this.lastVisited.splice(0, this.lastVisited.length == 5 ? 1 : 0, item);
                var names = this.lastVisited.map((element) => {
                    return element.name;
                });
                localStorage.setItem("ann_last_visited", JSON.stringify(names));
            }
        }
    }

    visitFile(item) {
        this.addLastVisited(item);
		Rest.getFileFromGitLab(item.name, (file) => {					
			EventDispatcher.fire(Constants.EVENT_ANNOTATIONS, {
				data: JSON.parse(file), 
				edited: false
			});
		}, (status) => {
			
		});
    }

    getUriArg(key) {
		var raw = window.location.hash.split('#');
		if(raw.length == 2) {
			var cmd = raw[1];
			var args = cmd.split('&');
			for(var i=0; i<args.length; ++i) {
				if(args[i].indexOf(key + "=") !== -1) {
					return args[i].split('=')[1]; 
				}
			}
		}
		return null;
    }

}

export default new Context;