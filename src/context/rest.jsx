import React from 'react';
import Constants from './constants.jsx';

class Rest {

    setupCallbacks(http, onSuccess, onError) {
        this.currentRequest = http;
        http.onerror = () => {
            if(onError != null) {
                onError(http.status);
            }
        }
        http.onload = () => {
            if(http.status >= 200 && http.status < 300) {
                if(onSuccess != null) {
                    try {
                        var response = http.response.split("\"taxonomy/").join("\"");
                        onSuccess(JSON.parse(response));
                    } catch(err) {
                        console.log("Exception", err);
                    }
                }
            } else {
                if(onError != null) {
                    onError(http.status);
                }
            }
        }
    }

    setupCallbacksRaw(http, onSuccess, onError) {
        this.currentRequest = http;
        http.onerror = () => {
            if(onError != null) {
                onError(http.status);
            }
        }
        http.onload = () => {
            if(http.status >= 200 && http.status < 300) {
                if(onSuccess != null) {
                    onSuccess(http.response);
                }
            } else {
                if(onError != null) {
                    onError(http.status);
                }
            }
        }
    }

    abort() {
        if(this.currentRequest) {
            this.currentRequest.abort();
        }
        if(this.currentErrorCallback) {
            this.currentErrorCallback(499); //Client Closed Request
        }
        this.currentRequest = null;
    }

    get(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", Constants.ANNOTATION_API_REST + func, true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    post(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", Constants.ANNOTATION_API_REST + func, true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    postBody(func, body, contentType, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", Constants.ANNOTATION_API_REST + func, true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        if(contentType) {
            http.setRequestHeader("Content-Type", contentType);
        }
        http.send(body);
    }

    postBody2(func, body, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", Constants.ANNOTATION_API_REST + func, true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send(body);
    }

    patch(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("PATCH", Constants.ANNOTATION_API_REST + func, true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    delete(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("DELETE", Constants.ANNOTATION_API_REST + func, true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    getPromise(func) {
		return new Promise((resolve, reject) => {
			var http = new XMLHttpRequest();
			http.onerror = () => {
				reject(http.status);
			}
			http.onload = () => {
				if(http.status >= 200 && http.status < 300) {
					try {
						var response = http.response.split("\"taxonomy/").join("\"");
						response = JSON.parse(response);
						resolve(response);
					} catch(err) {
						console.log("Exception", err);
					}
				} else {
					reject(http.status);
				}
			}
			http.open("GET", Constants.ANNOTATION_API_REST + func, true);
			http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
			http.setRequestHeader("Accept", "application/json");
			http.send();
		});
    }

    postPromise(func) {
		return new Promise((resolve, reject) => {
			var http = new XMLHttpRequest();
			http.onerror = () => {
				reject(http.status);
			}
			http.onload = () => {
				if(http.status >= 200 && http.status < 300) {
					try {
						var response = http.response.split("\"taxonomy/").join("\"");
						response = JSON.parse(response);
						resolve(response);
					} catch(err) {
						console.log("Exception", err);
					}
				} else {
					reject(http.status);
				}
			}
			http.open("POST", Constants.ANNOTATION_API_REST + func, true);
			http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
			http.setRequestHeader("Accept", "application/json");
			http.send();
		});
    }
    
    getGitlabPromise(func) {
		return new Promise((resolve, reject) => {
			var http = new XMLHttpRequest();
			http.onerror = () => {
				reject(http.status);
			}
			http.onload = () => {
				if(http.status >= 200 && http.status < 300) {
					try {
						resolve(JSON.parse(http.response));
					} catch(err) {
						console.log("Exception", err);
					}
				} else {
					reject(http.status);
				}
			}
			http.open("GET", func, true);
			http.setRequestHeader("Accept", "application/json");
			http.send();
		});
    }

    getVersionsPromis() {
		return this.getPromise("/main/versions");
    }

	getConcept(id, onSuccess, onError) {
		this.abort();
		var q = "query TaxonomyAnnotation { concepts(id: \"" + id + "\") { related(type: \"skill\") { id type preferred-label:preferred_label } } }";
		var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", Constants.TAXONOMY_API_REST + "/graphql?query=" + q, true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();		
	}

	autocomplete(text, onSuccess, onError) {
		this.abort();
		var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", Constants.TAXONOMY_API_REST + "/suggesters/autocomplete?version=next&query-string=" + text, true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();		
	}

    getTypes(onSuccess, onError) {
		//this.abort();
		var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", Constants.TAXONOMY_API_REST + "/main/concept/types", true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
	}

    postEducationDescription(text, types, onSuccess, onError) {
        this.abort();
        this.post("/nlp/education-description?text=" + encodeURIComponent(text) + (types ? "&types=" + types : ""), onSuccess, onError);
    }
    
    postEducationDescriptionPromise(text, types) {
        return this.postPromise("/nlp/education-description?text=" + encodeURIComponent(text) + (types ? "&types=" + types : ""));
    }

    postFileUpload(file, types, onSuccess, onError) {
        this.abort();
        var fd = new FormData();
        fd.append("file", file, file.name);
        this.postBody("/files/upload-education-description" + (types ? "&types=" + types : ""), fd, null, onSuccess, onError);
    }

    getGraphQL(query, onSuccess, onError) {
        var encodedQuery = encodeURIComponent("query TaxonomyAnnotation { " + query + " }");
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", Constants.TAXONOMY_API_REST + "/graphql?query=" + encodedQuery, true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    postJobAdAnnotations(id, types, onSuccess, onError) {
        this.post("/nlp/job-ad?id=" + id + (types ? "&types=" + types : ""), onSuccess, onError);
    }

    postRecommender(json, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("POST", Constants.RECOMMENDER_API_REST + "/semantic-concept-search/", true);
        http.setRequestHeader("api-key", Constants.TAXONOMY_API_REST_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.setRequestHeader("Content-Type", "application/json");
        http.send(JSON.stringify(json));        
    }

    saveAnnotatedText(data, onSuccess, onError) {
        this.abort();
        var saveObj = {
			"annotated-doc": data,
			"api-key": "EnHamsterÄrEnGodVänIEnsammaStunder33!"
		};
        var annotatedDoc = JSON.stringify(saveObj);        
        this.postBody("/private/save-annotated-text", annotatedDoc, "application/json", onSuccess, onError);
    }

    getSentimentTypes(onSuccess, onError) {
        this.get("/nlp/sentiment-types", onSuccess, onError);
    }

    getFilesFromGitLab(page, onSuccess, onError) {
        //this.abort();
		var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        http.open("GET", Constants.CORS_PROXY_URI + "/proxy/api/v4/projects/"+ Constants.GIT_PROJECT_ID + "/repository/tree?per_page=100&page=" + page, true);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    getFileFromGitLab(name, onSuccess, onError) {
        //this.abort();
		var http = new XMLHttpRequest();
        this.setupCallbacksRaw(http, onSuccess, onError);
        http.open("GET", Constants.CORS_PROXY_URI + "/proxy/api/v4/projects/"+ Constants.GIT_PROJECT_ID + "/repository/files/" + name + "/raw", true);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }
    
    getFileFromGitLabPromise(name) {
		return this.getGitlabPromise(Constants.CORS_PROXY_URI + "/proxy/api/v4/projects/"+ Constants.GIT_PROJECT_ID + "/repository/files/" + name + "/raw");
    }

    getTypesLocalization(onSuccessCallback, onError) {
        var onSuccess = (data) => {
            console.log(data);
            onSuccessCallback(data.concept_types);
        };
        var query = "concept_types(version: \"next\")"
            + "{ id label_en label_sv }";
        this.getGraphQL(query, onSuccess, onError);        
    }

}

export default new Rest;