
class Util { 

    toLowerCaseMeta(meta) {
        return meta.map((e) => {
            var key = Object.keys(e)[0];
            var obj = {};
            obj[key.toLowerCase()] = e[key];
            return obj;
        });
    }
	
    getMetaValue(meta, key, defaultValue) {
        if(meta != null) {
            if(Array.isArray(meta)) {
                var obj = meta.filter((element) => { return key == Object.keys(element)[0]; });            
                if(obj != null && obj.length > 0) {
                    return obj[0][key];
                }
            } else {
                var obj = meta[key];
                if(obj != null && obj.length > 0) {
                    return obj;
                }
            }
        }
        return defaultValue;
    }
    
    sortByKey(items, key, direction) {
        items.sort((a, b) => {
            return this.sortValue(direction, a[key], b[key]);
        });
        return items;
    }

    sortByCmp(items, cmp, direction) {
        items.sort((a, b) => {
            return this.sortValue(direction, cmp(a), cmp(b));
        });
        return items;
    }
    
    sortValue(direction, aa, bb) {
        var a = aa;
        var b = bb;
        if(typeof(a) === "string" && typeof(b) === "string") {
            a = a.toLowerCase();
            b = b.toLowerCase();
            if(direction) {
                return a.localeCompare(b);
            } else {
                return -a.localeCompare(b);
            }
        } else if(direction) {
            if(a < b) return -1;
            if(a > b) return 1;
        } else {
            if(a < b) return 1;
            if(a > b) return -1;
        }
        return 0;
    }

}

export default new Util;