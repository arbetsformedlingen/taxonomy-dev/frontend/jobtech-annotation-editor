
class Keybindings {

    constructor() {
        // constants
        this.BINDING_NEW_ANNOTATION = "key_new_annotation";
        this.BINDING_NEW_META = "key_new_meta";
        this.BINDING_SAVE_DOCUMENT = "key_save_document";
        this.BINDING_OPEN_ANNOTATION = "key_open_annotation";
        // members
        this.keys = [{
            type: this.BINDING_NEW_ANNOTATION,
            key: "a",
        }, {
            type: this.BINDING_NEW_META,
            key: "m",
        }, {
            type: this.BINDING_OPEN_ANNOTATION,
            key: "i",
        }, {
            type: this.BINDING_SAVE_DOCUMENT,
            key: "s",
        }];
    }

    getBindingForKey(key) {
        return this.keys.find((x) => { return x.key == key; });
    }

    onKeyBinding(e, type, propagateEvents) {
        if(e.ctrlKey && !e.repeat) {
            var binding = this.keys.find((x) => {
                return x.type == type;
            });
            if(e.key != null && binding && binding.key == e.key) {
                if(!propagateEvents) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                return true;
            }
        }
        return false;
    }

    save() {
        try {
            localStorage.setItem("ann_keybindings", JSON.stringify(this.keys));
        } catch(e) {

        }
    }

    load() {
        var k = localStorage.getItem("ann_keybindings");
        if(k != null) {
            try {
                // override existing keys
                var keys = JSON.parse(k);
                for(var i=0; i<keys.length; ++i) {
                    var binding = this.getBindingForKey(keys[i].type);
                    if(binding) {
                        binding.key = keys[i].key;
                    }
                }
            } catch(e) {

            }
        }
    }

}

export default new Keybindings;