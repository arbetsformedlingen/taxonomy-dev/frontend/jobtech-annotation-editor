import React from 'react';

class Constants { 

    constructor() {
        var createColors = () => {
            var c = [];
            for(var i=0; i<360; i+=10) {
                c.push("hsl(" + i + ",78%,86%)");
            }
            return c;
        };

        this.ICON_COPY_CLIPBOARD = <svg width="16px" height="16px" viewBox="0 0 16 16"><path xmlns="http://www.w3.org/2000/svg" d="M8 2.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM9.45 2a2.5 2.5 0 0 0-4.9 0H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h2v-1.5H3.5v-9h1V5h5V3.5h1V7H12V3a1 1 0 0 0-1-1H9.45zM7.5 9.5h1.25a.75.75 0 0 0 0-1.5h-1.5C6.56 8 6 8.56 6 9.25v1.5a.75.75 0 0 0 1.5 0V9.5zm1.25 5H7.5v-1.25a.75.75 0 0 0-1.5 0v1.5c0 .69.56 1.25 1.25 1.25h1.5a.75.75 0 0 0 0-1.5zm3.75-5h-1.25a.75.75 0 0 1 0-1.5h1.5c.69 0 1.25.56 1.25 1.25v1.5a.75.75 0 0 1-1.5 0V9.5zm-1.25 5h1.25v-1.25a.75.75 0 0 1 1.5 0v1.5c0 .69-.56 1.25-1.25 1.25h-1.5a.75.75 0 0 1 0-1.5z"/></svg>;

        // settings
		this.TAXONOMY_API_REST = window.g_app_taxonomy_api_path;
        this.TAXONOMY_API_REST_KEY = "111";

        this.ANNOTATION_API_REST = window.g_app_annotation_api_path;

        this.RECOMMENDER_API_REST = window.g_app_recommender_api_path;
        
        this.CORS_PROXY_URI = window.g_app_cors_path;

        this.GIT_PROJECT_ID = window.g_app_git_project_id;

        this.MENU_BUTTON_NEW_FILE = "new_file";
        this.MENU_BUTTON_SELECT_FILE = "select_file";
        this.MENU_BUTTON_ANNOTATION = "annotation";
        this.MENU_BUTTON_SETTINGS = "settings";

        this.EVENT_MENU_BUTTON = "event_menu_button";
        this.EVENT_ANNOTATIONS = "event_annotations";
        this.EVENT_SET_DOCUMENT = "event_set_document";

        this.ANNOTATION_COLORS = createColors();
    }

}

export default new Constants;