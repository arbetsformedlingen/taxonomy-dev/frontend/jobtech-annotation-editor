import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Button from "./button"
import React from "react"

describe("Button", () => {
  test("renders", () => {
    render(<Button />);
    expect(Button).toBeInstanceOf(Function);
    expect(Button).toHaveProperty("prototype");
    expect(Button.prototype).toBeInstanceOf(Object);
    expect(Button.prototype.constructor).toBe(Button);
  });
});