import React from 'react';
import ReactDOM from 'react-dom';
import Rest from './context/rest.jsx';
import Util from './context/util.jsx';
import Loader from './loader.jsx';
import Button from './controls/button.jsx';
import Context from './context/context.jsx';

class EditUploadList extends React.Component { 

	constructor(props) {
        super(props);
        this.state = {
            isImporting: false,
            files: [],
        };
		this.boundGlobalInput = this.onGlobalInput.bind(this);
    }

    componentDidMount() {
		document.addEventListener('keydown', this.boundGlobalInput);
    }

    componentWillUnmount() {
		document.removeEventListener('keydown', this.boundGlobalInput);
    }

	onGlobalInput(e) {
        if(e.key == "Escape") {
            e.preventDefault();
            e.stopPropagation();
            this.onClose();
		}
    }

    async readFile(file) {
        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload = (e) => {
            // read filenames and trim each line
            var data = e.target.result.split("\n").map((element) => {
                return element.trim();
            });
            data = data.filter(Boolean);
            // find files
            var files = [];
            for(var i=0; i<data.length; ++i) {
                var item = Context.getFile(data[i]);
                if(item) {
                    files.push(item);
                }
            }
            this.setState({
                files: files,
                isImporting: false,
            });
        };
    }

    onDragEnter(e) {
        e.preventDefault();
    }

    onDragOver(e) {
        e.preventDefault();
    }

    onDrop(e) {
        e.preventDefault();
        var file = e.dataTransfer.files[0];
        this.setState({isImporting: true}, () => {
            this.readFile(file);
        });
		// reset component, wont be able to upload new file if we dont do this
		e.target.value = '';
    }

    onFilePicker(e) {
        var file = e.target.files[0];      
        this.setState({isImporting: true}, () => {
            this.readFile(file);
        });
		// reset component, wont be able to upload new file if we dont do this
		e.target.value = '';
    }

    onProcess() {
        Context.processIndex = 0;
        Context.processFiles = this.state.files;
        if(Context.processFiles.length > 0) {
            Context.visitFile(Context.processFiles[0]);
        }
    }

    onSelectFile() {
        var filePicker = document.getElementById("file_picker");
        filePicker.click();
    }

    onClose() {
        this.props.onCloseDialog();
    }

    renderFilenames() {
        var items = this.state.files.map((item, i) => {
            var name = Util.getMetaValue(item.meta, "name", item.name);
			if(name.length == 0) {
				name = item.name;
			}
			return (
				<div
					key={i} 
					className="choose_file_column">
					{name}
				</div>
			);
        });
        return (
            <div className="edit_upload_list_content">
                <div className="edit_upload_list_content_items">
                    {items}
                </div>
				<div className="button_row">
					<Button
						onClick={this.onProcess.bind(this)}
						text="Bearbeta"/>
					<Button
						onClick={this.onClose.bind(this)}
						text="Stäng"/>
				</div>
            </div>
        );
    }

    renderUpload() {
        return (
            <div className="edit_upload_list_content">
                <div
                    className="choose_import_drag_drop"
                    id="drag_drop"
                    accept=".txt"
                    onDragEnter={this.onDragEnter.bind(this)}
                    onDragOver={this.onDragOver.bind(this)}
                    onDrop={this.onDrop.bind(this)}>    
                    {!this.state.isImporting &&
                        <div 
                            className="choose_import_dropzone"
                            id="dropzone">
                            Dra och släpp filer här (.txt)
                        </div>
                    }
                    {this.state.isImporting &&
                        <Loader />
                    }                                          
                </div> 
				<div className="button_row">
					<Button
						onClick={this.onSelectFile.bind(this)}
						text="Välj fil"/>
					<Button
						onClick={this.onClose.bind(this)}
						text="Stäng"/>
				</div>
                <input 
                    id="file_picker" 
                    className="choose_import_button_file_picker" 
                    type="file"
                    accept=".txt"
                    onChange={this.onFilePicker.bind(this)} />
            </div>
        );
    }

    render() {
        return (
            <div>
                <div className="edit_upload_list">
                    <div className="title">Bearbeta annonser</div>
                    {this.state.files.length == 0 &&
                        this.renderUpload()
                    }
                    {this.state.files.length > 0 &&
                        this.renderFilenames()
                    }
                </div>
            </div>
        );
    }
	
}

export default EditUploadList;