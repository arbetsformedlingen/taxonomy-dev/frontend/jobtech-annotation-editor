import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import AnnotateSettings from "./annotate_settings"
import React from "react"

describe("AnnotateSettings", () => {
  test("renders", () => {
    render(<AnnotateSettings />);
    expect(screen.getByText(/Kortkommandon/)).toBeDefined();
    expect(screen.getByText(/Klicka på ett kortkommando och tryck/)).toBeDefined();
    expect(screen.getByText(/Ny annotering/)).toBeDefined();
    expect(screen.getByText(/Nytt meta värde/)).toBeDefined();
    expect(screen.getAllByRole("heading", { hidden: true })).toBeDefined();
    expect(AnnotateSettings).toBeInstanceOf(Function);
    expect(AnnotateSettings).toHaveProperty("prototype");
    expect(AnnotateSettings.prototype).toBeInstanceOf(Object);
    expect(AnnotateSettings.prototype.constructor).toBe(AnnotateSettings);
  });
});