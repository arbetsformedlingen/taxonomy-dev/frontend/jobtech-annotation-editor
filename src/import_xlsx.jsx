import React from 'react';
import ReactDOM from 'react-dom';
import Rest from './context/rest.jsx';
import Util from './context/util.jsx';
import Loader from './loader.jsx';
import Button from './controls/button.jsx';
import Context from './context/context.jsx';
import ExcelLib from 'exceljs';

class ImportXlsx extends React.Component { 

	constructor(props) {
        super(props);
        // constants
        this.TYPE_IGNORE = "ignore";
        this.TYPE_NAME = "name";
        this.TYPE_TEXT = "text";
        this.TYPE_META = "meta";
        // state
        this.state = {
            current: 0,
            max: 0,
            isLoading: false,
            isComplete: false,
            headlines: [],
            rows: [],
        };
        this.importing = false;
    }

    componentDidMount() {
        var workbook = new ExcelLib.Workbook();
        var reader = new FileReader();
        reader.readAsArrayBuffer(this.props.file);
        reader.onload = () => {
            workbook.xlsx.load(reader.result).then((workbook) => {
                var worksheet = workbook.getWorksheet(1);
                var headlines = null;
                var rows = [];
                worksheet.eachRow({includeEmpty: false}, (row) => {
                    if(headlines == null) {
                        headlines = [];
                        for(var i=1; i<row.values.length; ++i) {
                            headlines.push({
                                title: row.values[i],
                                type: this.TYPE_IGNORE,
                            });
                        }
                    } else {
                        var values = row.values;
                        values.splice(0, 1);
                        rows.push(values);
                    }
                });
                this.setState({
                    headlines: headlines,
                    rows: rows,
                });
            });
        };
    }

    componentWillUnmount() {
        this.importing = false;
    }

    async onImportClicked() {
        if(this.state.rows.length == 0) {
            // nothing to import
            return;
        }
        this.importing = true;
        var extractElement = (text, index) => {
            if(text[index] == '"') {
                for(var i=index+1; i<text.length; ++i) {
                    if(text[i] == '"') {
                        // reached end of element, extract value
                        return { 
                            value: text.substring(index + 1, i),
                            index: i + 2, 
                        };
                    }
                }
            }
            return null;
        };
        var files = [];
        for(var i=0; i<this.state.rows.length; ++i) {
            if(!this.importing) {
                // user have clicked close, stop importing
                return;
            }
            // update indicator
            this.setState({
                isLoading: true,
                current: i + 1,
                max: this.state.rows.length,
            });
            var row = this.state.rows[i];
            var text = null;
            // find text
            for(var j=0; j<this.state.headlines.length; ++j) {
                if(this.state.headlines[j].type == this.TYPE_TEXT) {
                    text = row[j];
                    break;
                }
            }
            if(text) {
                // create document
                var r = await Rest.postEducationDescriptionPromise(text, Context.getTypes());
                files.push(r.sha1 + ".json");
                // update document
                r.meta = {
                    name: "",
                    type: "",
                    description: ""
                };
                for(var j=0; j<this.state.headlines.length; ++j) {
                    var headline = this.state.headlines[j];
                    if(headline.type == this.TYPE_NAME && row[j]) {
                        // row might contain a pure number force create a string
                        r.meta.name = ("" + row[j]).trim();
                    } else if(headline.type == this.TYPE_META && row[j]) {
                        // row might contain a pure number force create a string
                        var meta = ("" + row[j]).trim();
                        if(meta[0] == '[' && meta[meta.length - 1] == ']' ) {
                            // extract array values and reformat
                            meta = meta.substring(1, meta.length);
                            var rawIndex = 0;
                            var formatedMeta = [];
                            while(rawIndex < meta.length) {
                                var element = extractElement(meta, rawIndex);
                                if(element) {
                                    formatedMeta.push(element.value);
                                    rawIndex = element.index;
                                } else {
                                    rawIndex = meta.length;
                                }
                            }
                            // restore array format
                            meta = JSON.stringify(formatedMeta);
                        }
                        r.meta[headline.title] = meta;
                    }
                }
                Rest.saveAnnotatedText(r, () => {
                    // file saved, add file to context list
                    r.name = r.sha1 + ".json";
                    Context.files.push(r);
                }, (status) => {
                    console.log("failed to save imported document, code: " + status);
                });
            } else {
                // no text to process, proceed to next item
            }
        }
        // import done
        var text = files.join('\n');
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', "importerade filer.txt");
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
        this.setState({isComplete: true});
    }

    onColumnTypeChange(header, e) {
        header.type = e.target.value;
        this.forceUpdate();
    }

    render() {
        var headlines = this.state.headlines.map((element, index) => {
            return (
                <div
                    className="import_xlsx_row" 
                    key={index}>
                    <div className="import_xlsx_column_id">{String.fromCharCode(65 + index)}</div>
                    <select 
                        value={element.type}
                        onChange={this.onColumnTypeChange.bind(this, element)}>
                        <option value={this.TYPE_IGNORE}>Ignorera</option>
                        <option value={this.TYPE_NAME}>Namn</option>
                        <option value={this.TYPE_TEXT}>Text</option>
                        <option value={this.TYPE_META}>Meta</option>
                    </select>
                    <div>{element.title}</div>
                </div>
            );
        });
        return (
            <div>
                <div className="import_xlsx">
                    <div className="title">{this.props.file.name}</div>
                    {this.state.isComplete &&
                        <div className="import_complete">Import klar!</div>
                    }
                    {this.state.isLoading && !this.state.isComplete &&
                        <Loader text={"Importerar... (" + this.state.current + "/" + this.state.max + ")"}/>
                    }
                    {!this.state.isLoading &&
                        <div className="dialog_group">
                            <div className="title">Kolumner</div>
                            <div className="import_xlsx_rows">{headlines}</div>
                        </div>
                    }
					<div className="button_row">
						<Button
                            isEnabled={!this.state.isLoading}
							onClick={this.onImportClicked.bind(this)}
							text="Importera"/>
						<Button
							onClick={this.props.onCloseDialog.bind(this)}
							text="Stäng"/>
					</div>
                </div>
            </div>
        );
    }
	
}

export default ImportXlsx;