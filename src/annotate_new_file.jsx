import React from 'react';
import Rest from './context/rest.jsx';
import Button from './controls/button.jsx';
import EventDispatcher from './context/event_dispatcher.jsx';
import Constants from './context/constants.jsx';
import Context from './context/context.jsx';
import AnnotateLoading from './annotate_loading.jsx';
import ImportXlsx from './import_xlsx.jsx';

class AnnotateNewFile extends React.Component { 

	constructor() {
        super();
        // state
        this.state = {
			text: "",
			url: "",
			loading: false,
			isImportingXlsx: false,	
			xlsxFile: null,
        };
		this.firstSelected = null;
        this.boundGlobalDragEnter = this.onGlobalDragEnter.bind(this);
        this.boundGlobalDragOver = this.onGlobalDragOver.bind(this);
        this.boundGlobalDrop = this.onGlobalDrop.bind(this);
    }

    componentDidMount() {
        this.enableFileDrag();
    }

    componentWillUnmount() {
        this.disableFileDrag();
    }

	enableFileDrag() {
        window.addEventListener("dragenter", this.boundGlobalDragEnter, false);
        window.addEventListener("dragover", this.boundGlobalDragOver);
        window.addEventListener("drop", this.boundGlobalDrop);
    }

    disableFileDrag() {
        window.removeEventListener("dragenter", this.boundGlobalDragEnter);
        window.removeEventListener("dragover", this.boundGlobalDragOver);
        window.removeEventListener("drop", this.boundGlobalDrop);
    }

	sendFile(file) {
		this.setState({loading: true}, () => {
			Rest.postFileUpload(file, Context.getTypes(), (data) => {
				EventDispatcher.fire(Constants.EVENT_ANNOTATIONS, {data: data, edited: true, isNew: true});
			}, (status) => {
				this.setState({loading: false});
			});
		});
	}

	onFile(file) {
		if(file.type == "text/plain") {
			var reader = new FileReader();
			reader.readAsText(file);
			reader.onload = (e) => {
				this.onFromText(e.target.result);
			};
		} else if(file.type == "application/pdf") {
			this.sendFile(file);	
		} else if(file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
			this.setState({
				isImportingXlsx: true,
				xlsxFile: file,
			});
		}
	}

	onTextChanged(e) {
		this.setState({text: e.target.value});
	}
	
	onUrlChanged(e) {
		this.setState({url: e.target.value});
	}

	onGlobalDragEnter(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onGlobalDragOver(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onGlobalDrop(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onDragEnter(e) {
        e.preventDefault();
    }

    onDragOver(e) {
        e.preventDefault();
    }

    onDrop(e) {
        e.preventDefault();
        var file = e.dataTransfer.files[0];
        this.onFile(file);
		// reset component, wont be able to upload new file if we dont do this
		e.target.value = '';
    }

    onFilePicker(e) {
        var file = e.target.files[0];      
        this.onFile(file);
		// reset component, wont be able to upload new file if we dont do this
		e.target.value = '';
    }

    onChooseFileClicked() {
        var filePicker = document.getElementById("file_picker");
        filePicker.click();
    }

	onFromText(text) {
		if(text.length > 0) {
			this.setState({loading: true}, () => {
				Rest.postEducationDescription(text, Context.getTypes(), (data) => {
					EventDispatcher.fire(Constants.EVENT_ANNOTATIONS, {data: data, edited: true, isNew: true});
				}, (status) => {
					this.setState({loading: false});
				});
			});
		}
	}

	onFromUrl() {
		var url = this.state.url.trim();
		var id = null;
		if(url.indexOf('/') != -1) {
			var segments = url.split('/');
			id = segments[segments.length - 1].trim();
		} else {
			id = url.trim();
		}
		if(id != null) {
			Rest.postJobAdAnnotations(id, Context.getTypes(), (data) => {
				EventDispatcher.fire(Constants.EVENT_ANNOTATIONS, {data: data, edited: true, isNew: true});
			}, () => {

			});
		}
	}
	
	renderImportXlsxDialog() {
		return (
			<div className="edit_overlay">
				<div className="edit_dialog">
					<ImportXlsx
						file={this.state.xlsxFile}
						onCloseDialog={() => {
							this.setState({
								isImportingXlsx: false,
								xlsxFile: null,
							})
						}}/>
				</div>
			</div>
		);
	}

	renderContent() {
		if(this.state.loading) {
			return ( <AnnotateLoading/> );
		} else {
			return (
				<div className="new_container">
					<div>
						<div>Från text</div>
						<div className="search">
							<textarea 
								className="rounded"
								type="text"
								value={this.state.text}
								placeholder="Text att annotera"
								onChange={this.onTextChanged.bind(this)}
							/>
						</div>
						<div className="button_row">
							<Button
								onClick={this.onFromText.bind(this, this.state.text)}
								text="Kör"/>
						</div>
					</div>
					<div>
						<div>
							<div>Från platsannons (ange url / id)</div>
							<div className="search">
								<input 
									className="rounded"
									type="text"
									value={this.state.url}
									onChange={this.onUrlChanged.bind(this)}/>
							</div>
							<div className="button_row">
								<Button
									onClick={this.onFromUrl.bind(this)}
									text="Kör"/>
							</div>
						</div>
						<div>
							Från fil
							<div
								className="choose_import_drag_drop"
								id="drag_drop"
								accept=".pdf"
								onDragEnter={this.onDragEnter.bind(this)}
								onDragOver={this.onDragOver.bind(this)}
								onDrop={this.onDrop.bind(this)}>
									<div 
										className="choose_import_dropzone"
										id="dropzone">
										Dra och släpp här (.txt, .pdf, .xlsx)
									</div> 
							</div>
							<div className="button_row">
								<Button
									onClick={this.onChooseFileClicked.bind(this)}
									text="Välj fil"/>							
							</div>
							<input 
								id="file_picker" 
								className="choose_import_button_file_picker" 
								type="file"
								accept=".pdf,.txt,.xlsx"
								onChange={this.onFilePicker.bind(this)} />
						</div>
					</div>
					{this.state.isImportingXlsx &&
						this.renderImportXlsxDialog()
					}
				</div>
			);
		}
	}

    render() {
        return (
			<div className="content">
				<div className="group">
					<h3>Skapa ny</h3>
					{this.renderContent()}
				</div>
			</div>
        );
    }
	
}

export default AnnotateNewFile