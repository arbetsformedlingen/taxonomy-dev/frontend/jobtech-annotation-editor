import React from 'react';
import Rest from './context/rest.jsx';
import Button from './controls/button.jsx';
import EventDispatcher from './context/event_dispatcher.jsx';
import Constants from './context/constants.jsx';
import Keybindings from './context/key_bindings.jsx'

class AnnotateSettings extends React.Component { 

	constructor() {
        super();
        // state
        this.state = {
        };
    }

    getText(type) {
        var text = {};
        text[Keybindings.BINDING_NEW_ANNOTATION] = "Ny annotering (för markerad text)";
        text[Keybindings.BINDING_NEW_META] = "Nytt meta värde";
        text[Keybindings.BINDING_OPEN_ANNOTATION] = "Öppna annotering";
        text[Keybindings.BINDING_SAVE_DOCUMENT] = "Spara";
        return text[type];
    }

    onItemClicked(id) {
        document.getElementById(id).focus();
    }

    onKeyDown(item, e) {
        if(e.key == "Escape") {
            e.target.blur();
        } else if(e.key >= 'a' && e.key <= 'ö') {
            // if another keybinding with this key exists, remove it
            var binding = Keybindings.getBindingForKey(e.key);
            if(binding != null) {
                binding.key = null;
            }
            // update current
            item.key = e.key;
            // save bindings
            Keybindings.save();
        } else {
            // TODO: notify invalid key
        }
        this.forceUpdate();
    }

    renderItems() {
        return Keybindings.keys.map((item, index) => {
            return (
                <div 
                    className="settings_keybinding"
                    key={index}
                    onPointerUp={this.onItemClicked.bind(this, "key_" + index)}>
                    <div>{this.getText(item.type)}</div>
                    <div>
                        <div>CTRL +</div>
                        <div 
                            className="settings_keybinding_key"
                            id={"key_" + index}
                            tabIndex="0"
                            onKeyDown={this.onKeyDown.bind(this, item)}>
                            {item.key}
                        </div>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
			<div className="content">
				<div className="group">
					<h3>Kortkommandon</h3>
                    <div>Klicka på ett kortkommando och tryck sedan på önskad knapp för att binda om kommandot</div>
                    <div className="settings_keybindings">
                        {this.renderItems()}
                    </div>
				</div>
			</div>
        );
    }
	
}

export default AnnotateSettings;