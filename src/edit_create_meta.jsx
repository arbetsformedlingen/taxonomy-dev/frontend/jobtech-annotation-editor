import React from 'react';
import ReactDOM from 'react-dom';
import Rest from './context/rest.jsx';
import Util from './context/util.jsx';
import Context from './context/context.jsx';
import Button from './controls/button.jsx';

class EditCreateMeta extends React.Component { 

	constructor(props) {
        super(props);
        // state
        this.state = {
			name: "",
            isValid: false,
        };
		this.boundGlobalInput = this.onGlobalInput.bind(this);
    }

    componentDidMount() {
        document.getElementById("meta_name").focus();
		document.addEventListener('keydown', this.boundGlobalInput);
    }

    componentWillUnmount() {
		document.removeEventListener('keydown', this.boundGlobalInput);
    }

	onGlobalInput(e) {
        if(e.key == "Enter" && this.state.isValid) {
            e.preventDefault();
            e.stopPropagation();
            this.onSave();
		}
    }

    onNameChanged(e) {
        var value = e.target.value;
        this.setState({
            name: value,
            isValid: this.props.meta[value] == undefined,
        });
    }

    onClose() {
        this.props.onCloseDialog();
    }

    onSave() {
        this.props.meta[this.state.name] = "";
        this.props.onSave();
        this.props.onCloseDialog();
    }

    render() {
        return (
			<div className="meta_dialog">
				<div className="title">Ny meta</div>
				<div className="dialog_content">
                    <input 
                        id="meta_name"
                        type="text"
                        value={this.state.name}
                        onChange={this.onNameChanged.bind(this)}/>
				</div>
				<div className="button_row">
					<Button
						css="save"
						isEnabled={this.state.isValid}
						onClick={this.onSave.bind(this)}
						text="Spara"/>
					<Button
						onClick={this.onClose.bind(this)}
						text="Stäng"/>
				</div>
			</div>
        );
    }
	
}

export default EditCreateMeta;