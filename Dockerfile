FROM node:22.11.0-bookworm-slim as builder

COPY . /app
WORKDIR /app

# this part just makes sure there arent any compile errors
RUN apt-get update -y &&\
    npm install -g npm@9.8.1 &&\
    npm install &&\
    npm run release

FROM node:22.11.0-bookworm-slim as runner
ENV NODE_ENV=production
    
COPY --from=builder /app/src /app/src/
COPY --from=builder /app/index.html /app/config.js /app/package.json /app/
COPY --from=builder /app/node_modules /app/node_modules/

WORKDIR /app
RUN apt-get update -y && npm install -g

EXPOSE 8000
COPY config.js  /mnt/volumemount/config.js
RUN ln -sf /mnt/volumemount/config.js config.js

CMD npm run prod
